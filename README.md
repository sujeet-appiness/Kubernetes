###***Deployment of the application on Kubernetes Cluster***###

**Minikube**

#installation of kubectl

curl -LO https://storage.googleapis.com/kubernetes-release/release/v1.9.0/bin/linux/amd64/kubectl

sudo chmod +x ./kubectl

sudo mv ./kubectl /usr/local/bin/kubectl

#installation of minicube

curl -Lo minikube https://storage.googleapis.com/minikube/releases/v0.24.1/minikube-linux-amd64 && chmod +x minikube && sudo mv minikube /usr/local/bin/

minikube version

#Start minikube with vm-driver=none:

sudo minikube start --vm-driver=none

alias minikube-start='sudo minikube start --vm-driver=none'

export CHANGE_MINIKUBE_NONE_USER=true

sudo -E minikube start --vm-driver=none

echo ‘export CHANGE_MINIKUBE_NONE_USER=true’ >> ~/.bashrc

kubectl cluster-info

Kubernetes master is running at https://127.0.0.1:8443

[minikube link](https://medium.com/@vovaprivalov/setup-minikube-on-virtualbox-7cba363ca3bc)
