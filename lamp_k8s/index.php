<h1>Hello MySql</h1>
<h4>Attempting MySQL connection from php...</h4>
<?php
$host = 'mysql';
$user = '';
$pass = '';
$db = '';
$conn = new mysqli($host, $user, $pass, $db);

if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
} else {
    echo "Connected to MySQL successfully!";
}

?>
